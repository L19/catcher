﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D), typeof(CircleCollider2D))]
public class EggController : MonoBehaviour
{
    // public
    public float easiness = 100.0f; // キャッチ成功のしやすさを表すパラメータ

    // private
    private CircleCollider2D eggCircleCollider2D;
    private Vector3 firstCollisionPosition;
    private bool firstCollision = true;
    private bool alive = true;

    void Awake ()
    {
        eggCircleCollider2D = GetComponent<CircleCollider2D> ();
    }

    // Use this for initialization
    void Start ()
    {
        rigidbody2D.gravityScale = Random.Range (1.0f, 1.5f);

        eggCircleCollider2D.isTrigger = true;
    }
	
    // Update is called once per frame
    void Update ()
    {
	    
    }

    void OnTriggerEnter2D (Collider2D col)
    {
        if (col.tag == "Player" && alive) {
            // 初めて衝突したときの座標を記憶しておく
            if (firstCollision) {
                firstCollisionPosition = col.transform.position;
                firstCollision = false;
            }
        } else if (col.tag == "Ground" && alive) {
            Miss ();
        } else if (col.tag == "Destroyer") {
            Destroy (gameObject);
        }
    }

    void OnTriggerStay2D (Collider2D col)
    {
        // 記憶していた座標とこのメソッドが呼ばれた時の座標とを用いて、
        // かごの速度ベクトルを計算
        // 卵の速度ベクトルと差をとり、その大きさを優しさの度合いとしている
        if (!firstCollision && alive) {
            float deff = ((Vector3)rigidbody2D.velocity
                         - (col.transform.position - firstCollisionPosition) * easiness).magnitude;
            Debug.Log (deff);
            if (deff < 10.0f) {
                Success (col);
            } else {
                Miss ();
            }
        }
    }

    void Success (Collider2D col)
    {
        Score s = col.GetComponent<Score> ();
        s.score++;
        Destroy (gameObject);
    }

    // 失敗時のアニメーションを表現するメソッド
    void Miss ()
    {
        float random = Random.Range (-3.0f, 3.0f);
        rigidbody2D.velocity = Vector2.right * random + Vector2.up * 10.0f;
        rigidbody2D.angularVelocity = random * -100.0f;
        rigidbody2D.gravityScale = 3.0f;
        transform.renderer.sortingLayerName = "Egg";
        alive = false;
    }
}
