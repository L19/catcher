﻿using UnityEngine;
using System.Collections;

public class Emitter : MonoBehaviour
{
    // public
    public GameObject[] eggPrefab;

    public float interval = 3.0f;

    // Use this for initialization
    void Start ()
    {
        StartCoroutine ("Emit");
    }

    private IEnumerator Emit ()
    {
        while (true) {
            Vector2 pos = new Vector2 (Random.Range (-2.0f, 2.0f), transform.position.y);
            Instantiate (eggPrefab[(int)(Random.value * 3)], pos, Quaternion.identity);
            yield return new WaitForSeconds (interval);
        }
    }
	
    // Update is called once per frame
    void Update ()
    {
	
    }
}
