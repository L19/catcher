﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D), typeof(BoxCollider2D))]
public class PlayerController : MonoBehaviour
{
    // public
    public float speed = 5.0f;


    // private
    private BoxCollider2D playerBoxCollider2D;

    void Awake ()
    {
        playerBoxCollider2D = GetComponent<BoxCollider2D> ();
    }


    // Use this for initialization
    void Start ()
    {
        rigidbody2D.isKinematic = true;

        playerBoxCollider2D.isTrigger = true;
    }
	
    // Update is called once per frame
    void Update ()
    {
        if (Input.GetButton ("Horizontal") || Input.GetButton ("Vertical")) {
            float dx = Input.GetAxis ("Horizontal");
            float dy = Input.GetAxis ("Vertical");
            Move (new Vector2 (dx, dy));
        }
    }

    void Move (Vector2 direction)
    {
        Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0.2f, 0.1f));
        Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (0.8f, 0.5f));

        Vector2 pos = transform.position;
        pos += direction * speed * Time.deltaTime;

        pos.x = Mathf.Clamp (pos.x, min.x, max.x);
        pos.y = Mathf.Clamp (pos.y, min.y, max.y);

        transform.position = pos;
    }
}
